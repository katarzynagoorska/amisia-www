var gulp = require("gulp");
var sass = require("gulp-sass");
var minify = require("gulp-minify");
var concat = require("gulp-concat");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();

gulp.task("browserSync", function(done) {
  browserSync.init({
    server: "./dist",
    notify: false,
    open: true
  });
  done();
});

// CSS

gulp.task("sass", function(done) {
  return gulp
    .src("./dev/scss/styles.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions", "> 5%"]
      })
    )

    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist/css"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );    
  done();
});


// JS

gulp.task("js", function(done) {
  return gulp
    .src("./dev/js/**/*.js")
    .pipe(concat("app.js"))
    .pipe(minify())
    .pipe(gulp.dest("./dist/js"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
  done();
});


// CSS CONCAT

gulp.task("allCSS", function(done) {
  return gulp
    .src([
      "./node_modules/bootstrap/dist/css/bootstrap.min.css",
      "./node_modules/owl.carousel/dist/assets/owl.carousel.min.css",
      "./node_modules/owl.carousel/dist/assets/owl.theme.default.css",
      "./dist/css/styles.css"
    ])

    .pipe(concat("all.css"))
    .pipe(gulp.dest("./dist/css"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
  done();
});

// JS CONCAT

gulp.task("allJS", function(done) {
  return gulp
    .src([
      "./node_modules/jquery/dist/jquery.min.js",
      "./node_modules/popper.js/dist/umd/popper.min.js",
      "./node_modules/bootstrap/dist/js/bootstrap.min.js",
      "./node_modules/owl.carousel/dist/owl.carousel.min.js",
      "./dist/js/app-min.js"
    ])
    .pipe(concat("all.js"))
    .pipe(gulp.dest("./dist/js"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
  done();
});

gulp.task("watch", function(done) {

  gulp.watch("./dev/scss/**/*.scss", gulp.series("sass","allCSS"));

  gulp.watch("./dist/**/*.html").on("change", browserSync.reload);

  gulp.watch("./dev/js/**/*.js", gulp.series("js", "allJS"));


  done();
});

gulp.task(
  "default",
  gulp.series("browserSync", "watch", function(done) {
    // do more stuff
    done();
  })
);
