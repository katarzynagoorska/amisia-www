var main = {

    _init: function() {


        this.transformSVGToInline();

    },

    transformSVGToInline: function() {

        $('img.svg').each(function() {

            var $img = $(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgStyle = $img.attr('style');
            var imgURL = $img.attr('src');

            $.get(imgURL, function(data) {

                var $svg = $(data).find('svg');

                if (typeof imgID !== 'undefined')
                    $svg = $svg.attr('id', imgID);

                if (typeof imgClass !== 'undefined')
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');

                if (typeof imgStyle !== 'undefined') {
                    $svg = $svg.attr('style', imgStyle);

                    $svg.find('*').attr('style', imgStyle);
                }

                $svg = $svg.removeAttr('xmlns:a');
                $img.replaceWith($svg);

            }, 'xml');

        });

    },
};


$(document).ready(function() {

    main._init();
});