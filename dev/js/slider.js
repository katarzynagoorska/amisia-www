var slider = {
    _init: function() {
        this.initSlider(".baner-carousel", {
            loop: true,
            nav:true,
            dots: true,
            items: 1,
            autoplayHoverPause:true,
            autoplay: true,
            autoplayTimeout:3900,
            smartSpeed: 800
            

        });

        this.initSlider(".product-carousel", {
            items: 4,
            margin: 30,
            loop: true,
            nav:true,
            slideBy: 4,
            dots: false,

            autoplayHoverPause:true,
            autoplay: true,
            smartSpeed: 250,
            responsive:{
                0:{
                    items:1,
                    nav:false,
                },
                600:{
                    items:3,
                    nav:false,
                },
                1439:{
                    items:5,
                    nav:false
                }
            }

        })

        this.initSlider(".firms-carousel", {
            items: 7,
            margin: 30,
            loop: true,
            nav:true,
            slideBy: 1,
            dots: false,

            autoplayHoverPause:true,
            autoplay: true,
            smartSpeed: 250,
            responsive:{
                0:{
                    items:3,
                    nav:false,
                },
                600:{
                    items:6,
                    nav:false,
                },
                1439:{
                    items:5,
                    nav:false
                }
            }
        })

    },

    initSlider: function (className, options) {
        if ($(className).length === 0)
            return false;
        $(className).owlCarousel(options);
    },

};

$(document).ready(function() {
    slider._init();
});