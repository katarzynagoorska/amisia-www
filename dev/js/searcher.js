var searcher = {
    searcherContainer: null,

    _init: function () {
        this.searcherContainer = $('#search-results');
        if (this.searcherContainer.length === 0)
            return false;

        this.getData();
    },

    getData: function () {
        var self = this;
        $.get( "data/wyszukiwarka.json")
        .done(function(data) {
            if (!data.products.length)
                return false;

            self.searchForProducts(data.products);
        });
    },

    searchForProducts: function (products) {
        var searchQuery = window.location.search.split("search=")[1].replace('+', ' ');

        for (var key in products) {
            var product = products[key];
            if (product.name.indexOf(searchQuery) >= 0) {
                this.addNewProduct(product)
            }
        }
    },

    addNewProduct: function (product) {
        this.searcherContainer.append('<section><h1>' + product.name + '</h1><img src="' + product.photo + '"><div>Cena: ' + product.price + '</div></section>');
    }
};

$(document).ready(function() {
    searcher._init();
});