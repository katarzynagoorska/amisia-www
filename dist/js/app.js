var main = {

    _init: function() {


        this.transformSVGToInline();

    },

    transformSVGToInline: function() {

        $('img.svg').each(function() {

            var $img = $(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgStyle = $img.attr('style');
            var imgURL = $img.attr('src');

            $.get(imgURL, function(data) {

                var $svg = $(data).find('svg');

                if (typeof imgID !== 'undefined')
                    $svg = $svg.attr('id', imgID);

                if (typeof imgClass !== 'undefined')
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');

                if (typeof imgStyle !== 'undefined') {
                    $svg = $svg.attr('style', imgStyle);

                    $svg.find('*').attr('style', imgStyle);
                }

                $svg = $svg.removeAttr('xmlns:a');
                $img.replaceWith($svg);

            }, 'xml');

        });

    },
};


$(document).ready(function() {

    main._init();
});
var searcher = {
    searcherContainer: null,

    _init: function () {
        this.searcherContainer = $('#search-results');
        if (this.searcherContainer.length === 0)
            return false;

        this.getData();
    },

    getData: function () {
        var self = this;
        $.get( "data/wyszukiwarka.json")
        .done(function(data) {
            if (!data.products.length)
                return false;

            self.searchForProducts(data.products);
        });
    },

    searchForProducts: function (products) {
        var searchQuery = window.location.search.split("search=")[1].replace('+', ' ');

        for (var key in products) {
            var product = products[key];
            if (product.name.indexOf(searchQuery) >= 0) {
                this.addNewProduct(product)
            }
        }
    },

    addNewProduct: function (product) {
        this.searcherContainer.append('<section><h1>' + product.name + '</h1><img src="' + product.photo + '"><div>Cena: ' + product.price + '</div></section>');
    }
};

$(document).ready(function() {
    searcher._init();
});
var slider = {
    _init: function() {
        this.initSlider(".baner-carousel", {
            loop: true,
            nav:true,
            dots: true,
            items: 1,
            autoplayHoverPause:true,
            autoplay: true,
            autoplayTimeout:3900,
            smartSpeed: 800
            

        });

        this.initSlider(".product-carousel", {
            items: 4,
            margin: 30,
            loop: true,
            nav:true,
            slideBy: 4,
            dots: false,

            autoplayHoverPause:true,
            autoplay: true,
            smartSpeed: 250,
            responsive:{
                0:{
                    items:1,
                    nav:false,
                },
                600:{
                    items:3,
                    nav:false,
                },
                1439:{
                    items:5,
                    nav:false
                }
            }

        })

        this.initSlider(".firms-carousel", {
            items: 7,
            margin: 30,
            loop: true,
            nav:true,
            slideBy: 1,
            dots: false,

            autoplayHoverPause:true,
            autoplay: true,
            smartSpeed: 250,
            responsive:{
                0:{
                    items:3,
                    nav:false,
                },
                600:{
                    items:6,
                    nav:false,
                },
                1439:{
                    items:5,
                    nav:false
                }
            }
        })

    },

    initSlider: function (className, options) {
        if ($(className).length === 0)
            return false;
        $(className).owlCarousel(options);
    },

};

$(document).ready(function() {
    slider._init();
});